---
layout: page
title: "Introduction"
author: RaReNet
language: en
summary: "The Digital First Aid Kit aims to provide preliminary support for people facing the most common types of digital threats. The Kit offers a set of self-diagnostic tools for human rights defenders, bloggers, activists and journalists facing attacks themselves, as well as providing guidelines for digital first responders to assist a person under threat."
date: 2015-08
permalink: /en/
parent: Home
---
# Welcome to the Digital First Aid Kit (DFAK)!

DFAK is a free and open tool that helps human rights defenders, bloggers, activists, and journalists better protect their communities against the most common types of digital threats. If you are assisting someone under threats, this kit teaches you how to address the cause of problem through a set of questions and matches it with possible solutions, including expert help desks to go to and external resources to consult. You will also learn [how to connect with the right people securely](https://dfak.org/securecomms).  

## Who is DFAK for?

DFAK is a powerful tool for you *if you are helping people secure themselves and their work online and protecting them from digital attacks.* It eases your way to assess the problem and search for advanced help.

## How to use DFAK?

Starting with a problem, DFAK walks you through a set of questions to better define the cause of problem, and recommends useful guides and external resources for you. For problems you cannot tackle at the end of the journey, DFAK will have a short list of expert teams for you to consult or connect people in need with. 

First, describe the problem you are tackling. You are helping someone who:

- lost access to their accounts
- lost control of their devices
- may be under phishing or malware attack 
- had a website that is not working
- had someone impersonating them online
- or got harassed online 

## Who is behind DFAK?

DFAK is a collaborative effort of EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, IWPR, Open Technology Fund, and individual security experts who are working in the field of digital security and rapid response. It is a work in progress and we [welcome any comments, suggestions, or questions](url that directs people to GitLab or something).

## What else you need to know?

*DFAK is not meant to serve as the ultimate solution to all your digital emergencies.* If you feel uncertain about how to address the problem or implement a solution in helping others troubleshoot using DFAK, feel free to ask for help from [trained professionals](https://www.dfak.org/list-of-experts) in [secure ways](https://dfak.org/securecomms). You can also refer to [this list of glossary](url to the glossary) to clarify terms. 

## Feeling overwhelmed?

One line to link to the dedicated page.